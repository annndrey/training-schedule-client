// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
const axiosConfig = {
    timeout: 30000,
    withCredentials: false
};

import Vue from 'vue'
import axios from 'axios'
import VueFlashMessage from 'vue-flash-message';
import App from './App'
import router from './router'
import store from './store'
import VModal from 'vue-js-modal'
import ToggleButton from 'vue-js-toggle-button'

Vue.config.productionTip = false

// Setting up Axios on Vue Instance, for use via this.$axios
Vue.prototype.$axios = axios.create(axiosConfig);
// Default vars set up from localStorage (ie, user has come back)
//const token = localStorage.getItem('token')
//if (token) {
Vue.prototype.$axios.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem('token')}`;
Vue.prototype.$backendhost = 'http://trololo.info:6546/api/v1/'
//}
const moment = require('moment')
require('moment/locale/ru')
require('vue-flash-message/dist/vue-flash-message.min.css');
Vue.use(require('vue-moment'), {
    moment
})
Vue.use(VueFlashMessage, { timeout: 2000 });
Vue.use(VModal)
Vue.use(ToggleButton)

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
})
