import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/HelloWorld'
import Admin from '@/components/Admin'
import Trainings from '@/components/Trainings'
import Login from '@/components/Login'
import Logout from '@/components/Logout'
import Groups from '@/components/Groups'
import Timetable from '@/components/Timetable'
import Students from '@/components/Students'
import Student from '@/components/Student'
import Visits from '@/components/Visits'
import ImportData from '@/components/ImportData'
import Lessons from '@/components/Lessons'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
	{
	    path: '/',
	    name: 'Main',
	    component: Main,
	    beforeEnter: (to, from, next) => {
		// ...
		const currentUser = JSON.parse(window.localStorage.getItem('user'))
		if ( currentUser.isadmin && !currentUser.superadmin) {
		    next('/admin')
		}
		next()
	    }
	},
	{
	    path: '/importdata',
	    name: 'ImportData',
	    component: ImportData,
	    beforeEnter: (to, from, next) => {
		// ...
		const currentUser = JSON.parse(window.localStorage.getItem('user'))
		if (currentUser.istrainer) {
		    next('/timetable')
		}
		next()
	    }
	},
	{
	    path: '/admin',
	    name: 'Admin',
	    component: Admin,
	    beforeEnter: (to, from, next) => {
		// ...
		const currentUser = JSON.parse(window.localStorage.getItem('user'))
		if (currentUser.istrainer && !currentUser.superadmin && !currentUser.admin) {
		    next('/timetable')
		}
		next()
	    }
	},
	{
	    path: '/lessons',
	    name: 'Lessons',
	    component: Lessons,
	    beforeEnter: (to, from, next) => {
		// ...
		const currentUser = JSON.parse(window.localStorage.getItem('user'))
		if (currentUser.istrainer) {
		    next('/timetable')
		}
		next()
	    }
	    
	},
	{
	    path: '/trainings',
	    name: 'Trainings',
	    component: Trainings,
	    beforeEnter: (to, from, next) => {
		// ...
		const currentUser = JSON.parse(window.localStorage.getItem('user'))
		if (currentUser.istrainer  && !currentUser.superadmin && !currentUser.admin) {
		    next('/timetable')
		}
		next()
	    }
	    
	},
	{
	    path: '/visits',
	    name: 'Visits',
	    component: Visits,
	    beforeEnter: (to, from, next) => {
		// ...
		const currentUser = JSON.parse(window.localStorage.getItem('user'))
		if (currentUser.istrainer  && !currentUser.superadmin && !currentUser.admin) {
		    next('/timetable')
		}
		next()
	    }

	},
	{
	    path: '/timetable',
	    name: 'Timetable',
	    component: Timetable,
	    beforeEnter: (to, from, next) => {
		// ...
		const currentUser = JSON.parse(window.localStorage.getItem('user'))
		if (currentUser.superadmin && !currentUser.istrainer) {
		    next('/')
		} else if (currentUser.admin) {
		    next('admin')
		}
		next()
	    }

	},
	{
	    path: '/groups',
	    name: 'Groups',
	    component: Groups,
	    beforeEnter: (to, from, next) => {
		// ...
		const currentUser = JSON.parse(window.localStorage.getItem('user'))
		if (currentUser.istrainer) {
		    next('/timetable')
		}
		next()
	    }

	},
	{
	    path: '/students',
	    name: 'Students',
	    component: Students,
	    beforeEnter: (to, from, next) => {
		// ...
		const currentUser = JSON.parse(window.localStorage.getItem('user'))
		if (currentUser.istrainer  && !currentUser.superadmin && !currentUser.admin) {
		    next('/timetable')
		}
		next()
	    }

	},
	{
	    path: '/st/:id',
	    name: 'Student',
	    component: Student,
	    props: true,
	    beforeEnter: (to, from, next) => {
		// ...
		const currentUser = JSON.parse(window.localStorage.getItem('user'))
		if (currentUser.istrainer  && !currentUser.superadmin && !currentUser.admin) {
		    next('/timetable')
		}
		next()
	    }
	    
	},
	{
	    path: '/login',
	    name: 'Login',
	    component: Login
	},
	{
	    path: '/logout',
	    name: 'Logout',
	    component: Logout
	}
    ]
})

